import network


def connect_wifi(wifi_ssid: str, wifi_pass: str):
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    while not wlan.isconnected():
        nets = wlan.scan()
        for (ssid, *tail) in nets:
            if ssid.decode('utf-8') == wifi_ssid:
                print('connecting to network...')
                wlan.connect(wifi_ssid, wifi_pass)
                while not wlan.isconnected():
                    pass
                print('network config:', wlan.ifconfig())