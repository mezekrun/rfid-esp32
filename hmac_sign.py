def hmac_sign(key: str, method: str, endpoint: str):
    from machine import RTC
    import hmac
    from hashlib import sha512

    rtc = RTC()
    now = rtc.datetime()
    date = "{:02d}-{:02d}-{:02d} {:02d}:{:02d}:{:02d}.{:d}".format(now[0], now[1], now[2], now[4], now[5], now[6],
                                                                   now[7])
    data = "date: {}\n{} {}".format(date, method, endpoint)
    signature = hmac.new(
        key.encode(),
        data.encode(),
        sha512
    ).hexdigest()

    return date, signature
