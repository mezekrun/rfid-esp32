import ujson
import time
from machine import UART
import binascii
from server_request import add_timing
from wifi import connect_wifi

uart = UART(2, 115200)

with open('config.json', 'rt') as fp:
    config = ujson.loads(fp.read())

wifi = config.get('wifi')


def rfid():
    if uart.any() > 0:
        tag = binascii.hexlify(uart.readline()).decode()[14:-12].upper()
        process_tag(tag)


def process_tag(tag):
    connect_wifi(wifi.get('ssid'), wifi.get('pass'))
    add_timing(
        tag=tag,
        hmac_secret=config.get('hmac_secret'),
        server=config.get('server'),
        endpoint=config.get('endpoint'),
        method=config.get('method')
    )


connect_wifi(wifi.get('ssid'), wifi.get('pass'))
print('Listening for tags...')
while True:
    rfid()
    time.sleep_ms(100)
    pass
