# Supported boards
- ESP32 (Wroom)

# Deployment procedure
`cp config.json.example config.json`

Change hmac_secret to already inserted RFID reader in timing-app. Get RFID secret by running on timing-app `bin/console rfid:secret`