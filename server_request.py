import urequests as requests
from hmac_sign import hmac_sign


def add_timing(tag: str, hmac_secret: str, server: str, method: str, endpoint: str):
    date, signature = hmac_sign(key=hmac_secret, method=method, endpoint=endpoint)
    data = f'tag={tag}'
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': f'hmac-rfid algorithm="sha512", headers="Date", signature="{signature}"',
        'Date': date
    }
    res = requests.request(
        method=method,
        url=f'{server}{endpoint}',
        data=data,
        headers=headers,
    )
    print(res.text)